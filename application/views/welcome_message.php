<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Login</title>
        <?php $this->load->view('include/header'); ?>
    </head>
    <body class="bg-gradient-primary">
        <div class="container">
            <!-- Outer Row -->
            <div class="row justify-content-center">

                <div class="col-xl-10 col-lg-12 col-md-9">

                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-2 d-none d-lg-block "></div>
                                <div class="col-lg-8">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4">Login</h1>
                                        </div>
                                        <form class="user">
                                            <div class="form-group">
                                                <input type="email" class="form-control form-control-user" name="userEmail" id="userEmail" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="password" class="form-control form-control-user" id="password" placeholder="Password">
                                            </div>
                                            <div class="form-group">
                                                <div class="custom-control custom-checkbox small">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck">
                                                </div>
                                            </div>
                                            <a href="#" style="color:white" id="loginbtn" onclick="login();" type="button" class="btn btn-primary btn-user btn-block">
                                                Login
                                            </a>
                                            <hr>
                                        </form>
                                        <div class="text-center">
                                            <a class="small" href="#">Forgot Password?</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 d-none d-lg-block "></div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
        <?php $this->load->view('include/footer_link'); ?>
    </body>
    <script>
        $baseUrl = $(".baseURL").val();
        function login() {
    //    if (LoginValidation()) {
            var obj = new Object()
            obj.password = $("#password").val()
            obj.userEmail = $('#userEmail').val()
            $.ajax({
                url: $baseUrl + "welcome/login",
                async: false,
                data: obj,
                type: 'POST',
                success: function (r) {
                    if (r.status == 'true') {
                        $url = $baseUrl + 'dashboard';
                        location.href = $url;
                        showMessage('success', r.message);
                    } else {
                        showMessage('danger', r.message);
                    }
                }
            })
    //    }
        }

        function LoginValidation() {
            error = false;
            $loginpassword = $("#password").val();
            $userEmail = $('#userEmail').val();
            if ($.trim($userEmail) == "" || $.trim($userEmail) == null) {
                error = true;
                $("#error_loginmobileNo").addClass("error_input");
                $("#error_loginmobileNo").text("Mobile Number is required");
            } else {
                error = false;
                $("#error_loginmobileNo").removeClass("error_input");
                $("#error_loginmobileNo").text("");
            }
            if ($.trim($loginpassword) == "" || $.trim($loginpassword) == null) {
                error = true;
                $("#error_loginPassword").addClass("error_input");
                $("#error_loginPassword").text("Password is required");
            } else {
                error = false;
                $("#error_loginPassword").removeClass("error_input");
                $("#error_loginPassword").text("");
            }
            return error;
        }
    </script>
</html>
