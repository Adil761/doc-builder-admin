(function () {
    showMessage = function (mode, msg) {
        classmode = 'alert-info';
        switch (mode) {
            case 'success':
                classmode = 'alert-success';
                break;
            case 'warning':
                classmode = 'alert-warning';
                break;
            case 'danger':
                classmode = 'alert-danger';
                break;
        }
        $('#alert-msg').html('<span class="success" >' + msg + '</span>').addClass(classmode);
        setTimeout(function () {
            $('#alert-msg').removeClass(classmode).html('');
        }, 3000)
    }

    formattedText = function (text) {
        return (text == null || text == '') ? '' : text.trim();
    }

    firstLettersCap = function (string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    getBaseUrl = function () {
        return 'http://localhost/admin_doc/';
    }
})();